import os.path
from django.shortcuts import render, redirect
from datetime import datetime
from sklearn.preprocessing import MinMaxScaler

import matplotlib.pyplot as plt
import numpy as np
import io
import collections
import pymongo
import pandas as pd
import urllib, base64
from keras.models import load_model
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from sklearn import datasets, linear_model

connect_string = "mongodb+srv://nasdaq:nasdaq@cluster0.ajvmj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
my_client = pymongo.MongoClient(connect_string)

sector = 'TECH'
model_file = sector.lower() + "_stock_prediction_model_mse_v1.h5"


def home(request):
    return render(request, "index.html", {})


@csrf_exempt
def show_stock_sentiment(request):
    start = '2021-01-01'
    end = '2021-12-31'
    ddl_code = 'NVDA'

    if request.method == "POST":
        print(request.POST)
        ddl_code = request.POST["stock_var"]
        start = request.POST["stock_start"]
        end = request.POST["stock_end"]

    # First define the database name
    dbname = my_client['nasdaq']

    # Now get/create collection name (remember that you will see the database in your mongodb cluster only after you create a collection
    collection_name = dbname["stock_details"]
    # print(collection_name)

    print(start)
    print(end)
    criteria = {"$and": [{"Date": {"$gte": start, "$lte": end}}, {"stock": ddl_code}]}
    print(criteria)
    # Read the documents
    stock_prediction = collection_name.find(criteria)

    df = pd.DataFrame(list(stock_prediction))
    df.reset_index(inplace=True, drop=False)
    df.index = df["Date"]

    stock_codes = collection_name.find()
    df_codes = pd.DataFrame(list(stock_codes))
    df_codes.reset_index(inplace=True, drop=False)
    stock_codes = df_codes['stock'].unique()

    uri_sentiment = ''
    uri_sklearn_1 = ''
    uri_sklearn_2 = ''
    has_data = 1

    if df.empty:
        print('DataFrame is empty!')
        has_data = 0
    else:
        uri_sentiment = show_sentiment_charts(df)
        uri_sklearn_1 = show_sklearnRegression_1_charts(df)
        uri_sklearn_2 = show_sklearnRegression_2_charts(df)

        # context = {'data_price_volume': uri_price_volume, 'data_sma': uri_sma, 'data_predict': uri_predict}

    context = {'data_sentiment': uri_sentiment, 'data_sklearn_1': uri_sklearn_1, 'data_sklearn_2': uri_sklearn_2, 'stock_codes': stock_codes, 'ddl_code': ddl_code, 'has_data': has_data, 'stock_start': start, 'stock_end': end}

    return render(request, 'show_stock_sentiment.html', context)


def show_sentiment_charts(df):
    # Show_price_volume_relation_charts():
    fig = plt.figure(figsize=(10, 5))


    # create figure and axis objects with subplots()
    fig, ax = plt.subplots()
    # make a plot
    ax.plot(df['Date'], df['returns'], color="red", marker="o")
    # set x-axis label
    ax.set_xlabel("Date", fontsize=12)
    plt.xticks(rotation=90, horizontalalignment='right', fontsize=10)
    # set y-axis label
    ax.set_ylabel("Returns", color="red", fontsize=12)

    # twin object for two different y-axis on the sample plot
    ax2 = ax.twinx()
    # make a plot with different y-axis using second axis object
    ax2.plot(df['Date'], df['sentiment'], color="blue", marker="o")
    ax2.set_ylabel("sentiment", color="blue", fontsize=12)

    # plt.show()
    # save the plot as a file
    # fig.savefig('two_different_y_axis_for_single_python_plot_with_twinx.jpg',
    #             format='jpeg',
    #             dpi=100,
    #             bbox_inches='tight')

    # plt.title('Close Price History')
    # top = plt.subplot2grid((4, 4), (0, 0), rowspan=3, colspan=4)
    # bottom = plt.subplot2grid((4, 4), (3, 0), rowspan=3, colspan=4)
    # top.plot(df.index, df['Adj Close'])
    # bottom.bar(df.index, df['Volume'])
    # # set the labels
    # top.axes.get_xaxis().set_visible(False)
    # top.set_title('Stock Price vs Volume')
    # top.set_xlabel('Date', fontsize=12)
    # top.set_ylabel('Close Price USD ($)', fontsize=12)
    # bottom.set_ylabel('Volume', fontsize=12)
    # plt.xticks(rotation=90, horizontalalignment='right', fontsize=10)
    # # plt.show()

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()

    return uri


def show_sklearnRegression_1_charts(df):

    yData = df["returns"].values.reshape(-1, 1)
    xData = df["sentiment"].values.reshape(-1, 1)

    yDataTraining = yData[: int(len(yData)*.8)]
    xDataTraining = xData[:int(len(xData)*.8)]

    regressionModel = linear_model.LinearRegression()
    regressionModel.fit(xDataTraining, yDataTraining)
    regressionModel.score(xDataTraining, yDataTraining)

    # Show_price_volume_relation_charts():
    fig = plt.figure(figsize=(10, 5))

    fig, ax = plt.subplots()

    plt.scatter(xData, yData, color="black")
    plt.plot(xDataTraining, regressionModel.predict(xDataTraining), color="blue", linewidth=3)
    plt.xticks(())
    plt.yticks(())
    # plt.show()

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()

    return uri


def show_sklearnRegression_2_charts(df):

    yData = df["returns"].values.reshape(-1, 1)
    xData = df["sentiment"].values.reshape(-1, 1)

    yDataTraining = yData[: int(len(yData)*.8)]
    yDataTest = yData[: int(len(yData) * .2)]

    xDataTraining = xData[:int(len(xData)*.8)]
    xDataTest = xData[:int(len(xData) * .2)]

    dateSet = df["Date"]
    dateSetTraining = dateSet[:int(len(dateSet) * .8)]
    dateSetTest = dateSet[:int(len(dateSet) * .2)]

    regressionModel = linear_model.LinearRegression()
    regressionModel.fit(xDataTraining, yDataTraining)
    regressionModel.score(xDataTraining, yDataTraining)


    y_pred = regressionModel.predict(xDataTest)
    #df = pd.DataFrame({'Actual': yDataTest, 'Predicted': y_pred})

    # create figure and axis objects with subplots()
    # Show_price_volume_relation_charts():
    fig = plt.figure(figsize=(10, 5))
    fig, ax = plt.subplots()
    # make a plot
    ax.plot(dateSetTest, yDataTest, color="red", marker="o")
    # set x-axis label
    ax.set_xlabel("Date", fontsize=12)
    plt.xticks(rotation=90, horizontalalignment='right', fontsize=10)

    # set y-axis label
    ax.set_ylabel("Actual", color="red", fontsize=12)

    # twin object for two different y-axis on the sample plot
    ax2 = ax.twinx()
    # make a plot with different y-axis using second axis object
    ax2.plot(dateSetTest, y_pred, color="blue", marker="o")
    ax2.set_ylabel("Predicted", color="blue", fontsize=12)
    # plt.show()
    # save the plot as a file
    # fig.savefig('two_different_y_axis_for_single_python_plot_with_twinx.jpg',
    #             format='jpeg',
    #             dpi=100,
    #             bbox_inches='tight')

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()
    return uri

