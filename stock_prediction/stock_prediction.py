import base64
import io
import os.path
import urllib
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from django.conf import settings
from django.shortcuts import render
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
from django.views.decorators.csrf import csrf_exempt

from stock_prediction.mongodb_functions import get_database

sector = 'TECH'
model_file = sector.lower() + "_stock_prediction_model_mse_v1.h5"


def home(request):
    return render(request, "dashboard.html", {})


@csrf_exempt
def show_stock_prediction(request):
    start = '2021-03-24'
    end = '2022-04-24'
    ddl_code = 'AAPL'

    if request.method == "POST":
        print(request.POST)
        ddl_code = request.POST["stock_var"]
        start = request.POST["stock_start"]
        end = request.POST["stock_end"]

    dbname = get_database('nasdaq')

    # Now get/create collection name (remember that you will see the database in your mongodb cluster only after you create a collection
    collection_name = dbname["stock_prediction_with_date"]

    # start_dt = datetime.strptime(start, "%Y-%m-%d")
    # end_dt = datetime.strptime(end, "%Y-%m-%d")
    start_dt = start
    end_dt = end

    # Read the documents
    criteria = {"$and": [{"Trade Date": {"$gte": start_dt, "$lte": end_dt}}, {"Ticker": ddl_code}]}
    print(criteria)
    stock_prediction = collection_name.find(criteria)
    df = pd.DataFrame(list(stock_prediction))
    df.reset_index(inplace=True, drop=False)

    print(df)

    stock_codes = collection_name.find()
    df_codes = pd.DataFrame(list(stock_codes))
    df_codes.reset_index(inplace=True, drop=False)
    stock_codes = df_codes['Ticker'].unique()

    uri_price_volume = ''
    uri_sma = ''
    uri_predict = ''
    has_data = 1

    if df.empty:
        print('DataFrame is empty!')
        has_data = 0
    else:
        # df.index = df["Trade Date"].dt.strftime('%Y-%m-%d')
        df.index = df["Trade Date"]
        uri_price_volume = show_price_volume_relation_charts(df)
        uri_sma = show_moving_average_charts(df)
        uri_predict = show_predictions_chart(df)

    context = {'data_price_volume': uri_price_volume, 'data_sma': uri_sma, 'data_predict': uri_predict, 'stock_codes': stock_codes, 'ddl_code': ddl_code, 'has_data': has_data, 'stock_start': start, 'stock_end': end}

    return render(request, 'show_stock_prediction.html', context)


def show_price_volume_relation_charts(df):
    # Show_price_volume_relation_charts():
    fig = plt.figure(figsize=(10, 5))

    plt.title('Close Price History')
    top = plt.subplot2grid((4, 4), (0, 0), rowspan=3, colspan=4)
    bottom = plt.subplot2grid((4, 4), (3, 0), rowspan=3, colspan=4)
    top.plot(df.index, df['Adj Close'])
    bottom.bar(df.index, df['Volume'])
    # set the labels
    top.axes.get_xaxis().set_visible(False)
    top.set_title('Stock Price vs Volume')
    top.set_xlabel('Trade Date', fontsize=12)
    top.set_ylabel('Close Price USD ($)', fontsize=12)
    bottom.set_ylabel('Volume', fontsize=12)
    plt.xticks(rotation=90, horizontalalignment='right', fontsize=10)
    # plt.show()

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()
    return uri


def show_moving_average_charts(df):
    # simple moving averages
    sma10 = df['Close'].rolling(5).mean()  # 10 days
    sma20 = df['Close'].rolling(10).mean()  # 20 days
    sma50 = df['Close'].rolling(21).mean()  # 50 days
    sma = pd.DataFrame({'Ticker': df['Close'], 'SMA 10': sma10, 'SMA 20': sma20, 'SMA 50': sma50})
    print('>>> Print show_moving_average_charts')
    print(sma)
    sma.plot(figsize=(10, 5), legend=True, title="Slow Moving Average")
    # plt.show()

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()
    return uri


def load_model_from_azure():
    # if RUN_LOCAL:
    model_file_path = "stock_prediction//models//" + model_file
    # model_file_path = static('models/' + model_file)
    # model_file_path = model_file
    file_ = open(os.path.join(settings.BASE_DIR, model_file_path))
    model = load_model(os.path.join(settings.BASE_DIR, model_file_path))
    print("Successfully loaded the trained model")
    return model
    # else:
    #     print(f"Downloading the stock prediction model from Azure container {model_container_name}")
    #     download_file(model_file, "models/", model_container_name)
    #     model = load_model("models/" + model_file)
    #     print(f"Successfully loaded model {model_file} from Azure container {model_container_name} for Ticker {ticker}")
    #     return model


def predict(df):
    # Training
    # Create a new dataframe with only the 'Close column
    data = df.filter(['Close'])
    # Convert the dataframe to a numpy array
    dataset = data.values
    # Get the number of rows to train the model on
    training_data_len = int(np.ceil(len(dataset) * .95))

    # Scale the data
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = scaler.fit_transform(dataset)

    # Create the training data set
    # Create the scaled training data set
    train_data = scaled_data[0:int(training_data_len), :]
    # Split the data into x_train and y_train data sets
    x_train = []
    y_train = []
    for i in range(60, len(train_data)):
        x_train.append(train_data[i - 60:i, 0])
        y_train.append(train_data[i, 0])
        if i <= 61:
            print(x_train)
            print(y_train)
            print()
    # Convert the x_train and y_train to numpy arrays
    x_train, y_train = np.array(x_train), np.array(y_train)
    # Reshape the data
    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
    print("X-training data: {}, Y-training data: {}".format(x_train.shape, y_train.shape))
    # Create the testing data set
    # Create a new array containing scaled values from index 1543 to 2002
    test_data = scaled_data[training_data_len - 60:, :]
    # Create the data sets x_test and y_test
    x_test = []
    y_test = dataset[training_data_len:, :]
    for i in range(60, len(test_data)):
        x_test.append(test_data[i - 60:i, 0])
    # Convert the data to a numpy array
    x_test = np.array(x_test)
    # Reshape the data
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

    # Get the models predicted price values
    trained_model = load_model_from_azure()
    predictions_df = trained_model.predict(x_test)
    predictions_df = scaler.inverse_transform(predictions_df)

    # Get the root mean squared error (RMSE)
    rmse = np.sqrt(np.mean(((predictions_df - y_test) ** 2)))

    print(f"Print root mean square error: {rmse}")
    return predictions_df


def show_predictions_chart(df):
    stock_predictions = predict(df)

    # Training
    # Create a new dataframe with only the 'Close column
    data = df.filter(['Close'])
    # Convert the dataframe to a numpy array
    dataset = data.values
    # Get the number of rows to train the model on
    training_data_len = int(np.ceil(len(dataset) * .95))

    # print(stock_predictions)

    # Plot the data
    train = data[:training_data_len]
    valid = data[training_data_len:]
    valid['Predictions'] = stock_predictions

    train['Predictions'] = None
    joined = train.append(valid)
    # print(joined)

    train = joined[joined.Predictions.isnull()]
    valid = joined[joined.Predictions.notnull()]

    # print(train)
    # print(valid)

    # Visualize the data
    plt.figure(figsize=(10, 5))
    plt.title('Stock Prediction Model')
    plt.xlabel('Trade Date', fontsize=12)
    plt.ylabel('Close Price USD ($)', fontsize=12)
    plt.plot(train['Close'])
    plt.plot(valid[['Close', 'Predictions']])
    plt.legend(['Train', 'Val', 'Predictions'], loc='lower left', fontsize=10)
    plt.xticks(rotation=90, horizontalalignment='right', fontsize=10)
    # plt.show()
    # Show the valid and predicted prices
    # print(f"Printing training data \n{train}")
    # print(f"Printing valid data \n{valid}")

    # Convert to image
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    plt.clf()
    plt.cla()
    plt.close()
    return uri

