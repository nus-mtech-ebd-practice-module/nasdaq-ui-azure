from pymongo import MongoClient

CONNECTION_STRING = "mongodb+srv://nasdaq:nasdaq@cluster0.ajvmj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"


def get_database(dbname):
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(CONNECTION_STRING)

    # Create the database for our example (we will use the same database throughout the tutorial
    return client[dbname]


def get_client():
    return MongoClient(CONNECTION_STRING)
