from django.urls import path

from stock_prediction.dashboard import show_dashboard
from stock_prediction.stock_news_sentiment_chart import show_stock_news_sentiment
from stock_prediction.stock_prediction import show_stock_prediction
from stock_prediction.stock_technicals import load_stock_technicals
from stock_prediction.stock_sentiment import show_stock_sentiment
from stock_prediction.stock_ta_analysis import load_stock_ta_analysis

urlpatterns = [
    path('', show_dashboard, name='show_dashboard'),
    path('show_dashboard/', show_dashboard, name='show_dashboard'),
    path('stock_news_analytics/', show_stock_news_sentiment, name='stock_news'),
    path('stock_ta_analysis/', load_stock_ta_analysis, name='stock_ta_analysis'),
    path('show_stock_prediction/', show_stock_prediction, name='show_stock_prediction'),
    path('value_investing_ta/', load_stock_technicals, name='load_stock_technicals'),
    path('show_stock_sentiment/', show_stock_sentiment, name='show_stock_sentiment'),
]

