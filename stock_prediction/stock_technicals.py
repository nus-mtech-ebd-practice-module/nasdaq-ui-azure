import base64
import io
import urllib
import os.path
from django.conf import settings
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from django.shortcuts import render

from stock_prediction.azure_blob_functions import download_file
from stock_prediction.mongodb_functions import get_database


# from azure_blob_functions import download_file
# from mongodb_functions import get_database


def load_stock_technicals(request):
    combined_stats = get_stock_fundamentals_from_db()
    combined_stats.columns = ["Attribute", "Recent", "Ticker"]

    pe_ratios = combined_stats[combined_stats["Attribute"] == "Trailing P/E"]
    pe_ratios['Recent'] = pe_ratios['Recent'].astype(float)

    # pe_ratios = pe_ratios.drop(columns=["level_0"])
    pe_ratios_sorted = pe_ratios.sort_values('Recent', kind="quicksort", ascending=True)
    print(len(pe_ratios_sorted))
    # print(pe_ratios_sorted)

    df = pe_ratios_sorted.to_html(header=True, escape=True,
                                  col_space=None, justify="left",
                                  classes="table table-striped", border=1, sparsify=False)

    plt.bar(pe_ratios_sorted['Ticker'], pe_ratios_sorted['Recent'])
    plt.xlabel('Stock Ticker')
    plt.ylabel('Trailing P/E')
    plt.xticks(rotation='vertical', ha='right', color='green', fontsize='7')
    # plt.show()

    plt.savefig("static/images/value_investing_image.png", bbox_inches="tight")
    plt.clf()
    plt.cla()
    plt.close()

    analyst_rating_df = load_analyst_ratings()
    analyst_rating_table = analyst_rating_df.to_html(header=True, escape=True,
                                                     col_space=None, justify="left",
                                                     classes="table table-striped", border=1, sparsify=False)

    context = {'value_ta_pe': df, 'analyst_rating_table': analyst_rating_table}

    return render(request, 'value_investing_ta.html', context)


def save_to_image_binary():
    # Convert to image
    pe_buf = io.BytesIO()
    plt.savefig(pe_buf, format='png')
    pe_buf.seek(0)
    pe_string = base64.b64encode(pe_buf.read())
    pe_ratios_image = urllib.parse.quote(pe_string)
    return pe_ratios_image


def get_stock_fundamentals_from_db():
    dbname = get_database('nasdaq')
    collection_name = dbname["stock_technicals"]
    # Read the documents
    stock_prediction = collection_name.find()
    combined_stats = pd.DataFrame(list(stock_prediction))
    combined_stats.reset_index(inplace=True, drop=False)
    combined_stats = combined_stats.drop(columns=['index', '_id'])
    combined_stats = combined_stats[combined_stats.Recent.notnull()]
    return combined_stats


def test_me():
    combined_stats = get_stock_fundamentals_from_db()
    combined_stats.columns = ["Attribute", "Recent", "Ticker"]

    pe_ratios = combined_stats[combined_stats["Attribute"] == "Trailing P/E"]
    pe_ratios['Recent'] = pe_ratios['Recent'].astype(float)

    # pe_ratios = pe_ratios.drop(columns=["level_0"])
    pe_ratios_sorted = pe_ratios.sort_values('Recent', kind="quicksort", ascending=True)
    print(len(pe_ratios_sorted))
    # print(pe_ratios_sorted)

    df = pe_ratios_sorted.to_html(header=True, escape=True,
                                  col_space=None, justify="left",
                                  classes="table table-striped", border=1, sparsify=False)

    plt.bar(pe_ratios_sorted['Ticker'], pe_ratios_sorted['Recent'])
    plt.xlabel('Stock Ticker')
    plt.ylabel('Trailing P/E')
    plt.autoscale()
    plt.xticks(rotation=90, ha='right', color='green', fontsize='7')
    # plt.show()


def load_analyst_ratings():
    analyst_rating_df = populate_analyst_rating_data()
    print("Successfully loaded the trained model")
    analyst_rating_df = analyst_rating_df.replace({np.nan: ''})
    analyst_rating_df = analyst_rating_df[['Date', 'Ticker', 'Firm', 'To Grade', 'From Grade', 'Action']]
    # print(analyst_rating_df)
    return analyst_rating_df


def populate_analyst_rating_data():
    # Below lines are to download from Azure
    # year = datetime.now().year
    # csv_analyst_ratings_yearly = f"datasets/analyst_ratings_{year}.csv"
    # azure_nasdaq_analyst_ratings_container = "nasdaq-analyst-ratings"
    # download_file(csv_analyst_ratings_yearly, "", azure_nasdaq_analyst_ratings_container)
    # downloaded_file_path = f"datasets/analyst_ratings_{year}.csv"
    # downloaded_file = open(os.path.join(settings.BASE_DIR, downloaded_file_path))
    # print(f"Successfully loaded the analyst rating file: {downloaded_file}")

    # model_file_path = "stock_prediction\\datasets\\analyst_ratings_2022.csv"
    model_file_path = "stock_prediction//datasets//analyst_ratings_2022.csv"
    analyst_rating_df = pd.read_csv(os.path.join(settings.BASE_DIR, model_file_path))
    return analyst_rating_df

# test_me()
# df = load_stock_technicals("")
# load_analyst_ratings()
# print(len(df))
