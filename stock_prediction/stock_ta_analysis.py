import base64
import io
import urllib
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd
import talib
from dateutil.relativedelta import relativedelta
from django.shortcuts import render
from matplotlib.pylab import date2num
from mplfinance.original_flavor import candlestick_ohlc
from django.views.decorators.csrf import csrf_exempt
import pymongo

# Get today's date as UTC timestamp
today = datetime.today().strftime("%d/%m/%Y")
today = datetime.strptime(today + " +0000", "%d/%m/%Y %z")
to = int(today.timestamp())
# Get date ten years ago as UTC timestamp
ten_yr_ago = today - relativedelta(years=10)
fro = int(ten_yr_ago.timestamp())

connect_string = "mongodb+srv://nasdaq:nasdaq@cluster0.ajvmj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
my_client = pymongo.MongoClient(connect_string)


@csrf_exempt
def load_stock_ta_analysis(request):
    start = '2021-01-01'
    end = '2021-12-31'
    ddl_code = 'NVDA'
    stock_obs = 90

    if request.method == "POST":
        print(request.POST)
        ddl_code = request.POST["stock_var"]
        start = request.POST["stock_start"]
        end = request.POST["stock_end"]
        stock_obs = int(request.POST["stock_obs"])

    # First define the database name
    dbname = my_client['nasdaq']

    # Now get/create collection name (remember that you will see the database in your mongodb cluster only after you create a collection
    collection_name = dbname["stock_technical_analysis"]
    # print(collection_name)

    print(start)
    print(end)
    criteria = {"Ticker": ddl_code}

    print(criteria)

    # Read the documents
    stock_data = collection_name.find(criteria)

    df_mongo = pd.DataFrame(list(stock_data))

    # select columns
    df = df_mongo[['Date', 'Open', 'High', 'Low', 'Close', 'Adj Close', 'Volume']]
    df.reset_index(inplace=True, drop=False)
    df.sort_values(by=['Date'])
    df = df.set_index(pd.DatetimeIndex(df['Date']))

    # print(df)

    stock_codes = collection_name.find()
    df_codes = pd.DataFrame(list(stock_codes))
    df_codes.reset_index(inplace=True, drop=False)
    stock_codes = df_codes['Ticker'].unique()

    uri_candlestick = ''
    has_data = 1

    if df.empty:
        print('DataFrame is empty!')
        has_data = 0
    else:
        uri_candlestick = ''

    context = {'data_candlestick': uri_candlestick, 'stock_codes': stock_codes, 'ddl_code': ddl_code, 'has_data': has_data, 'stock_start': start, 'stock_end': end, 'stock_obs': stock_obs}

    # nflx_df = get_price_hist(ddl_code)
    # nflx_df2 = get_indicators(nflx_df)

    nflx_df3 = get_indicators(df)

    plt = plot_chart(nflx_df3, stock_obs, ddl_code)
    return render(request, 'show_stock_ta_analysis.html', context)


# def get_price_hist(ticker):
#     # Put stock price data in dataframe
#     url = "https://query1.finance.yahoo.com/v7/finance/download/{ticker}?period1={fro}&period2={to}&interval=1d&events=history".format(
#         ticker=ticker, fro=fro, to=to)
#     data = pd.read_csv(url)
#
#     # Convert date to timestamp and make index
#     data.index = data["Date"].apply(lambda x: pd.Timestamp(x))
#     data.drop("Date", axis=1, inplace=True)
#
#     return data


def get_indicators(data):
    # Get MACD
    data["macd"], data["macd_signal"], data["macd_hist"] = talib.MACD(data['Close'])

    # Get MA10 and MA30
    data["ma10"] = talib.MA(data["Close"], timeperiod=10)
    data["ma30"] = talib.MA(data["Close"], timeperiod=30)

    # Get RSI
    data["rsi"] = talib.RSI(data["Close"])
    return data


def plot_chart(data, n, ticker):
    # Filter number of observations to plot
    data = data.iloc[-n:]

    # Create figure and set axes for subplots
    fig = plt.figure()
    fig.set_size_inches((20, 16))
    ax_candle = fig.add_axes((0, 0.72, 1, 0.32))
    ax_macd = fig.add_axes((0, 0.48, 1, 0.2), sharex=ax_candle)
    ax_rsi = fig.add_axes((0, 0.24, 1, 0.2), sharex=ax_candle)
    ax_vol = fig.add_axes((0, 0, 1, 0.2), sharex=ax_candle)

    # Format x-axis ticks as dates
    ax_candle.xaxis_date()

    # Get nested list of date, open, high, low and close prices
    ohlc = []
    for date, row in data.iterrows():
        openp, highp, lowp, closep = row[['Open', 'High', 'Low', 'Close']]
        ohlc.append([date2num(date), openp, highp, lowp, closep])

    # Plot candlestick chart
    ax_candle.plot(data.index, data["ma10"], label="MA10")
    ax_candle.plot(data.index, data["ma30"], label="MA30")
    candlestick_ohlc(ax_candle, ohlc, colorup="g", colordown="r", width=0.8)
    ax_candle.legend()

    # Plot MACD
    ax_macd.plot(data.index, data["macd"], label="macd")
    ax_macd.bar(data.index, data["macd_hist"] * 3, label="hist")
    ax_macd.plot(data.index, data["macd_signal"], label="signal")
    ax_macd.legend()

    # Plot RSI
    # Above 70% = overbought, below 30% = oversold
    ax_rsi.set_ylabel("(%)")
    ax_rsi.plot(data.index, [70] * len(data.index), label="overbought")
    ax_rsi.plot(data.index, [30] * len(data.index), label="oversold")
    ax_rsi.plot(data.index, data["rsi"], label="rsi")
    ax_rsi.legend()

    # Show volume in millions
    ax_vol.bar(data.index, data["Volume"] / 1000000)
    ax_vol.set_ylabel("(Million)")

    # Save the chart as PNG
    fig.savefig("static/images/ta_image.png", bbox_inches="tight")
    plt.clf()
    plt.cla()
    plt.close()
    # plt.show()
    return plt


# df = load_stock_technicals("")
# print(len(df))
