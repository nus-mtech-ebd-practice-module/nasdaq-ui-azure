import os

from azure.storage.blob import BlobServiceClient, ContainerClient, __version__

NASDAQ_TOP_STOCK_PREDICTION_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=nasdaqtopstockprediction;AccountKey=uSQWYr1bw3WhsgBVXvIwEqM4NkEygojUOgckz8PFHvaVNzbWRKQwpQx0dkoupfxkWu7BvIwVlhMJ+AStR7HD3Q==;EndpointSuffix=core.windows.net"
os.environ["AZURE_STORAGE_TOP_STOCK_PREDICTION_CONNECTION_STRING"] = NASDAQ_TOP_STOCK_PREDICTION_CONNECTION_STRING


def get_client():
    try:
        print("Azure Blob Storage v" + __version__ + " - Python quickstart sample")

        # Quick start code goes here

    except Exception as ex:
        print('Exception:')
        print(ex)

    connect_str = os.getenv('AZURE_STORAGE_TOP_STOCK_PREDICTION_CONNECTION_STRING')
    print("Environment: ", connect_str)
    # Create the BlobServiceClient object which will be used to create a container client
    return BlobServiceClient.from_connection_string(connect_str)


def get_container_client():
    try:
        print("Azure Blob Storage v" + __version__ + " - Python quickstart sample")

        connect_str = os.getenv('AZURE_STORAGE_TOP_STOCK_PREDICTION_CONNECTION_STRING')
        print("Environment: ", connect_str)

        # Create the BlobServiceClient object which will be used to create a container client
        return ContainerClient.from_connection_string(connect_str)

    except Exception as ex:
        print('Exception:')
        print(ex)


def create_container(container_name):
    container_name = container_name.lower()
    blob_service_client = get_client()
    print(f"container Name {container_name}")
    try:
        container_client = ContainerClient.from_connection_string(NASDAQ_TOP_STOCK_PREDICTION_CONNECTION_STRING, container_name)
        if container_client.exists():
            print(f"container Name {container_name} already exists, so not creating it")
        else:
            print(f"Creating container: {container_name}")
            container_client = blob_service_client.create_container(container_name)
    except Exception as ex:
        print('Exception:')
        print(ex)


def upload_file(filename, path, container_name):
    filename = filename.lower()
    container_name = container_name.lower()
    print(f"filename {filename}, path: {path}, container_name: {container_name}")
    blob_service_client = get_client()
    # Create a blob client using the local file name as the name for the blob
    blob_client = blob_service_client.get_blob_client(container=container_name, blob=filename)
    upload_file_path = os.path.join(path, filename)
    print("\nUploading to Azure Storage as blob:\n\t" + filename)

    # Upload the created file
    with open(upload_file_path, "rb") as data:
        blob_client.upload_blob(data, overwrite=True)


def list_blobs(container_name):
    print("\nListing blobs...")
    blob_service_client = get_client()
    # Instantiate a ContainerClient
    container_client = blob_service_client.get_container_client(container_name)

    # List the blobs in the container
    blob_list = container_client.list_blobs()
    for blob in blob_list:
        print("\t" + blob.name)


def download_file(filename, path, container_name):
    filename = filename.lower()
    container_name = container_name.lower()
    print(f"filename {filename}, path: {path}, container_name: {container_name}")

    blob_service_client = get_client()
    download_file_path = os.path.join(path, filename)

    print("\nDownloading blob to \n\t" + download_file_path)
    blob_client = blob_service_client.get_blob_client(container=container_name, blob=filename)
    with open(download_file_path, "wb") as file:
        file.write(blob_client.download_blob().readall())
