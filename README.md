# 1. Setup virtual environment
python -m venv venv
venv\Scripts\activate

# 2. Install library
pip install django
pip install pymongo[snappy,gssapi,srv,tls]
pip install dnspython
pip install matplotlib
pip install pandas
pip install bs4
pip install nltk
pip install keras
pip install tensorflow
pip install djangorestframework

pip install azure-storage-blob
python -m pip install dnspython
python -m pip install pymongo
pip install pandas_datareader
pip install azure
pip install TA_Lib-0.4.24-cp39-cp39-win_amd64.whl

source venv/Scripts/activate

# 3. Script to run Django
python manage.py runserver


python -m pip freeze > requirements.txt
python -m pip install -r requirements.txt

